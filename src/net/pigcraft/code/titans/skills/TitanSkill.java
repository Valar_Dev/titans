package net.pigcraft.code.titans.skills;

import net.pigcraft.code.titans.Titan;
import org.bukkit.event.Listener;

public abstract class TitanSkill implements Listener
{


    private final String name;
    private final Titan titan;

    private String description;

    public TitanSkill(final String name, final Titan titan)
    {
        this.name = name;
        this.titan = titan;

        this.description = "";
    }

    public final String getName()
    {
        return name;
    }

    public final Titan getTitan()
    {
        return titan;
    }

    public final String getDescription()
    {
        return description;
    }

    public final void setDescription(final String description)
    {
        this.description = description;
    }


    public abstract void use();

    @Override
    public final String toString()
    {
        return this.getTitan().getName() + "::" + this.getName();
    }

}
