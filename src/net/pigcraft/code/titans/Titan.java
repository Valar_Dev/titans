package net.pigcraft.code.titans;

import net.pigcraft.code.titans.skills.TitanSkill;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public abstract class Titan
{

    protected final Titans plugin;

    private Player controller;
    private String name;
    private String entranceMessage;
    private String exitMessage;
    private String displayFormat;
    private String description;

    private final ArrayList<TitanSkill> skills;

    public abstract void onEntrance();
    public abstract void onExit();

    public abstract void onLoad();
    public abstract void onUnload();


    public Titan(final Titans plugin, String name, String entranceMessage, String exitMessage, String displayFormat)
    {
        this.plugin = plugin;
        this.name = name;
        this.entranceMessage = entranceMessage;
        this.exitMessage = exitMessage;
        this.displayFormat = displayFormat;
        this.description = "";

        this.skills = new ArrayList<TitanSkill>();
    }

    public final String getName()
    {
        return name;
    }

    public final Player getController()
    {
        return controller;
    }

    public final String getEntranceMessage()
    {
        return entranceMessage;
    }

    public final String getExitMessage()
    {
        return exitMessage;
    }

    public final String getDisplayFormat()
    {
        return displayFormat;
    }

    public final String getDescription()
    {
        return description;
    }


    public final void addSkill(final TitanSkill skill)
    {
        if (this.skills.contains(skill))
        {
            Bukkit.getLogger().severe("Titan " + this.getName() + "tried to register duplicate skill " + skill.getName());
        }
        else
        {
            this.skills.add(skill);
        }
    }

    public final TitanSkill getSkill(final String skillName)
    {
        for (TitanSkill skill : this.skills)
        {
            if (skill.getName().equalsIgnoreCase(skillName))
            {
                return skill;
            }
        }

        return null;
    }

    public final void clearSkills()
    {
        if (this.skills != null)
        {
            this.skills.clear();
        }
    }

    public final ArrayList<TitanSkill> getSkills()
    {
        return this.skills;
    }




    public final void setController(final Player controller)
    {
        this.controller = controller;
    }

    public final void setName(String name)
    {
        this.name = name;
    }

    public final void setEntranceMessage(final String entranceMessage)
    {
        this.entranceMessage = entranceMessage;
    }

    public final void setExitMessage(final String exitMessage)
    {
        this.exitMessage = exitMessage;
    }

    public final void setDisplayFormat(final String displayFormat)
    {
        this.displayFormat = displayFormat;
    }

    public final boolean hasController()
    {
        return this.controller != null;
    }

    public final void setDescription(final String description)
    {
        this.description = description;
    }


}
