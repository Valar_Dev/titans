package net.pigcraft.code.titans;

import net.pigcraft.code.titans.builtins.another_test.Toirneach;
import net.pigcraft.code.titans.builtins.pika.PikaTitan;
import net.pigcraft.code.titans.builtins.test_titan.TestTitan;
import net.pigcraft.code.titans.commands.CommandTitan;
import net.pigcraft.code.titans.listeners.ChatListener;
import net.pigcraft.code.titans.listeners.PlayerQuitListener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

public final class Titans extends JavaPlugin
{
    /**
     * Singleton instance
     */
    public static Titans plugin;

    private static ArrayList<Titan> loadedTitans = new ArrayList<Titan>();
    private static ArrayList<Player> titanPlayers = new ArrayList<Player>();




    @Override
    public void onEnable()
    {
        plugin = this;

        this.loadTitans();

        for (Titan t : getLoadedTitans())
        {
            t.onLoad();
        }

        this.getCommand("titan").setExecutor(new CommandTitan());
        this.getServer().getPluginManager().registerEvents(new ChatListener(), this);
        this.getServer().getPluginManager().registerEvents(new PlayerQuitListener(), this);

    }

    @Override
    public void onDisable()
    {

        for (Titan t : getLoadedTitans())
        {
            t.onUnload();
        }

        loadedTitans.clear();
        titanPlayers.clear();
    }


    private void loadTitans()
    {
        getLoadedTitans().add(new TestTitan());
        getLoadedTitans().add(new Toirneach());
        getLoadedTitans().add(new PikaTitan());

        Bukkit.getLogger().info("Loaded " + getLoadedTitans().size() + " titans.");
    }

    public final boolean isPlayerATitan(final Player player)
    {
        return titanPlayers.contains(player);
    }

    //TODO Replace with HashMap?
    public final Titan getTitanAssignedToPlayer(final Player player)
    {
        for (Titan t : loadedTitans)
        {
            if (t.hasController())
            {
                if (t.getController().equals(player))
                {
                    return t;
                }
            }
        }

        return null;
    }


    public void setPlayerAsTitan(final Player player, final Titan titan)
    {
        if (!this.isPlayerATitan(player))
        {
            Bukkit.getServer().broadcastMessage(titan.getEntranceMessage());
            titan.setController(player);

            titan.getController().sendMessage(ChatColor.GRAY + "You have been transformed into the Titan " + ChatColor.AQUA + titan.getName() + "!");
            titanPlayers.add(player);
            titan.onEntrance();
        }
        else
        {
            player.sendMessage(ChatColor.RED + "You are already a titan!");
        }
    }

    public void removePlayerAsTitan(final Player player)
    {
        if (this.isPlayerATitan(player))
        {

            Titan titan = getTitanAssignedToPlayer(player);

            Bukkit.getServer().broadcastMessage(titan.getExitMessage());

            titanPlayers.remove(player);
            titan.getController().sendMessage(ChatColor.GREEN + "You are no longer a Titan!");
            titan.onExit();

            titan.setController(null);
        }
        else
        {
            player.sendMessage(ChatColor.RED + "You are not a titan!");
        }
    }


    public Titan getTitan(final String titanName)
    {
        for (int i = 0; i < getLoadedTitans().size(); i++)
        {
            if (getLoadedTitans().get(i).getName().equalsIgnoreCase(titanName))
            {
                return getLoadedTitans().get(i);
            }
        }

        return null;
    }

    public static ArrayList<Titan> getLoadedTitans()
    {
        return loadedTitans;
    }




}
