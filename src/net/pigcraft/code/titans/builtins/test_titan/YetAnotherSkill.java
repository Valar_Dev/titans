package net.pigcraft.code.titans.builtins.test_titan;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.TargetedTitanSkill;
import org.bukkit.ChatColor;

public final class YetAnotherSkill extends TargetedTitanSkill
{


    public YetAnotherSkill(Titan titan)
    {
        super("ignite", titan, 20);
    }

    @Override
    public void use()
    {


        if (this.getTargetedEntity() == null)
        {
            getTitan().getController().sendMessage(ChatColor.RED + "No target within range!");
            return;
        }
        else
        {
            this.setTarget(getTargetedEntity());
        }

        this.getTarget().setFireTicks(500);
    }
}
