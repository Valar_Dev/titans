package net.pigcraft.code.titans.builtins.test_titan;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;
import org.bukkit.potion.PotionEffectType;

public final class TestTitan extends Titan
{

    private double prevHealth;

    public TestTitan()
    {
        super(Titans.plugin, "test", "Test has entered", "Test has left", "[Test-Titan]: $m");
    }

    @Override
    public void onEntrance()
    {
        this.addSkill(new BasicSkill(this));
        this.addSkill(new AnotherSkill(this));
        this.addSkill(new YetAnotherSkill(this));
        this.addSkill(new ListSkill(this));


        this.prevHealth = this.getController().getMaxHealth();
        this.getController().setMaxHealth(25D);
    }

    @Override
    public void onExit()
    {
        this.clearSkills();
        this.getController().setMaxHealth(this.prevHealth);
        this.getController().removePotionEffect(PotionEffectType.JUMP);
    }

    @Override
    public void onLoad()
    {
        //
    }

    @Override
    public void onUnload()
    {
        //
    }

}
