package net.pigcraft.code.titans.builtins.test_titan;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.ToggleableTitanSkill;
import org.bukkit.ChatColor;

public class AnotherSkill extends ToggleableTitanSkill
{

    public AnotherSkill(Titan titan)
    {
        super("fly", titan);
    }

    @Override
    public void onActivate()
    {
       getTitan().getController().sendMessage(ChatColor.YELLOW + "You rise into the heavens");
       getTitan().getController().setAllowFlight(true);
       getTitan().getController().setFlying(true);
    }

    @Override
    public void onDeactivate()
    {
        getTitan().getController().sendMessage(ChatColor.YELLOW + "You fall to earth");
        getTitan().getController().setFlying(false);
        getTitan().getController().setAllowFlight(false);
    }
}
