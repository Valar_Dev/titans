package net.pigcraft.code.titans.builtins.test_titan;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.TitanSkill;
import org.bukkit.ChatColor;

public final class ListSkill extends TitanSkill
{
    public ListSkill(Titan titan)
    {
        super("list", titan);
    }

    @Override
    public void use()
    {
        String toReturn = "";
        int counter = 0;
        for( TitanSkill skill : this.getTitan().getSkills())
        {
            String[] parts = skill.toString().split("::", 2);
            toReturn = toReturn + parts[1] + ", ";
            counter += 1;
        }
        getTitan().getController().sendMessage(ChatColor.AQUA + "Your Titan, " + getTitan().getName() + ", has " + counter + " skills");
        getTitan().getController().sendMessage(ChatColor.AQUA + toReturn);
    }
}