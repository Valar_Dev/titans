package net.pigcraft.code.titans.builtins.test_titan;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.TitanSkill;
import org.bukkit.ChatColor;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public final class BasicSkill extends TitanSkill
{
    public BasicSkill(Titan titan)
    {
        super("basic", titan);
    }

    @Override
    public void use()
    {
        getTitan().getController().sendMessage(ChatColor.YELLOW + "You used skill #1!");
        getTitan().getController().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 1200, 5));
    }
}
