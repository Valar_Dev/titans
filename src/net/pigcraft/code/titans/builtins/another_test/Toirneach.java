package net.pigcraft.code.titans.builtins.another_test;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public final class Toirneach extends Titan
{
    private boolean wasStorming;
    private String prevPlayerName;

    public Toirneach()
    {
        super(
                Titans.plugin,
                "Toirneach",
                ChatColor.GREEN + "The earth shakes beneath your feet and the skies rumble as the Lord of the Heavens strides onto the battlefield",
                ChatColor.GREEN + "The skies begin to clear as Toirneach leaves the battlefield",
                ChatColor.GREEN + "Toirneach :" + ChatColor.GRAY + " $m"
        );

        this.setDescription("Toirneach, Lord of the Heavens [...]");
    }

    @Override
    public void onEntrance()
    {
        this.wasStorming = this.getController().getWorld().hasStorm();
        this.prevPlayerName = this.getController().getPlayerListName();

        this.getController().getWorld().setStorm(true);
        this.getController().getWorld().setWeatherDuration(2500);

        this.getController().setPlayerListName(ChatColor.GREEN + "Toirneach");
    }

    @Override
    public void onExit()
    {
        this.getController().getWorld().setStorm(this.wasStorming);
        this.getController().setPlayerListName(this.prevPlayerName);

        this.clearSkills();
    }

    @Override
    public void onLoad()
    {
        this.addSkill(new TestListenerSkill(this));

        /*if (this.getSkill("stormthorn") == null)
        {
            Bukkit.getLogger().info("DEBUG::SKILL_NULL");
            return;
        }

        if (Titans.plugin == null)
        {
            Bukkit.getLogger().info("DEBUG::PLGN_NULL");
            return;
        }

        if (Bukkit.getServer() == null)
        {
            Bukkit.getLogger().info("DEBUG::SVR_NULL");
            return;
        }

        try
        {
           this.plugin.getServer().getPluginManager();
        }
        catch (NullPointerException npe)
        {
            Bukkit.getLogger().info("DEBUG::MGR_NULL");
        } */

        this.plugin.getServer().getPluginManager().registerEvents(this.getSkill("stormthorn"), this.plugin);
    }

    @Override
    public void onUnload()
    {
        EntityDamageByEntityEvent.getHandlerList().unregister(this.getSkill("stormthorn"));
        this.clearSkills();
    }
}
