package net.pigcraft.code.titans.builtins.another_test;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.ToggleableTitanSkill;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public final class TestListenerSkill extends ToggleableTitanSkill
{

    public TestListenerSkill(Titan titan)
    {
        super("stormthorn", titan);
    }

    @Override
    public void onActivate()
    {

    }

    @Override
    public void onDeactivate()
    {

    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e)
    {
        if (this.isActivated())
        {
            e.getPlayer().sendMessage(ChatColor.BLUE + "You sent message " + e.getMessage());
        }
    }
}
