package net.pigcraft.code.titans.builtins.pika;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;

import org.bukkit.ChatColor;
import org.bukkit.potion.PotionEffectType;

public final class PikaTitan extends Titan
{

    private double prevHealth;

    public PikaTitan()
    {
        super(Titans.plugin, "Pika", "Pika...bzzt...chu!", " Chuuuuuuuuu... bzzt!", ChatColor.DARK_AQUA + "[Titan] " + ChatColor.WHITE + "Super Mecha Death Pika Version 4.0 Beta" + ChatColor.GREEN + "$m");
    }

    @Override
    public void onEntrance()
    {


        this.prevHealth = this.getController().getMaxHealth();
        this.getController().setMaxHealth(25D);
        this.getController().getWorld().strikeLightningEffect(this.getController().getLocation());
        //Attempted to do a Bukkit.ScheduleSyncRepeating task, but couldn't get it to work. 
        //Trying to make a way set more repeating harmless lightning around pikaTitan. 
    }

    @Override
    public void onExit()
    {
        this.clearSkills();
        this.getController().setMaxHealth(this.prevHealth);
        this.getController().removePotionEffect(PotionEffectType.JUMP);
    }

    @Override
    public void onLoad()
    {
        //
    }

    @Override
    public void onUnload()
    {
        //
    }

}
