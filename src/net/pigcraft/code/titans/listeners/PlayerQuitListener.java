package net.pigcraft.code.titans.listeners;

import net.pigcraft.code.titans.Titans;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener
{

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerLeave(PlayerQuitEvent e)
    {
        if (Titans.plugin.isPlayerATitan(e.getPlayer()))
        {
            Titans.plugin.removePlayerAsTitan(e.getPlayer());
        }
    }

}
