Titans is a plugin designed to give certain players superpowers (usually in a PvP/RPG setting). Currently, Titans are
created and configured by extending the Titan class and compiling the resulting code into a jar file. This will then be
loaded by the core framework at runtime.